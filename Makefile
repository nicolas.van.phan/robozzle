# Frontend to dune.

.PHONY: default build install uninstall test clean fmt
.IGNORE: fmt

default: build

build:
	dune build
	rm -rf app
	mkdir -p app/data
	cp _build/default/robozzle/bin/webserver.exe  app/
	cp _build/default/robozzle/bin/index.html     app/
	cp _build/default/robozzle/bin/robozzle.bc.js app/data/
	cp -r _build/default/robozzle/imgs            app/data/
	cp -r _build/default/robozzle/levels          app/data/
	echo -e "\nRun ./app/webserver.exe to run the game\n"

test:
	dune runtest -f

install:
	dune install

uninstall:
	dune uninstall

clean:
	rm -rf app
	dune clean
# Optionally, remove all files/folders ignored by git as defined
# in .gitignore (-X).
	git clean -dfXq

fmt:
	dune build @fmt
	dune promote
