
open Core_kernel
open Json_lexer
open Lexing
open Js_of_ocaml
open Js_of_ocaml_lwt
open Jsoo_defs

type color = Green | Blue | Yellow | Red | Empty

type orientation = Up | Down | Left | Right

type coordinates = int * int

type robot_state = {
  pos : coordinates;
  dir : orientation;
}

type t = {
  grid : color Adj.matrix;
  robot : robot_state;
  functions : (string * int) list;
  stars : (int * int) list;
  canvas : (int * int);
}

(* ---------- Functions to build the Level.t from the JSON AST ---------- *)

let functions_from_ast ast =
  List.map ast ~f:(fun elt ->
    match elt with
    | `Assoc [ ("name", `String name); ("size", `Int size) ] -> (name, size)
    | _ -> fprintf stderr "Json : Wrong format for robot function"; exit (-1)
    )

let robot_pos_from_ast ast =
  match ast with
  | [`Int x; `Int y] -> (x, y)
  | _ -> fprintf stderr "Json : Wrong format for robot position"; exit (-1)

let robot_dir_from_ast ast =
  match ast with
  | "up" -> Up
  | "left" -> Left
  | "right" -> Right
  | "down" -> Down
  | _ -> fprintf stderr "Json : Wrong value %s for robot direction" ast; exit (-1)

let stars_from_ast ast =
  List.map ast ~f:(fun elt ->
    match elt with
    | `List [ `Int x; `Int y ] -> (x, y)
    | _ -> fprintf stderr "Json : Wrong format for star position"; exit (-1)
    )

let grid_row_from_ast ast =
  let explode s = List.init (String.length s) ~f:(String.get s) in
  match ast with
  | `String str ->
    List.filter_mapi (explode str) ~f:(fun i c -> match c with
    | 'r' -> Some (i, Red)
    | 'y' -> Some (i, Yellow)
    | 'g' -> Some (i, Green)
    | 'b' -> Some (i, Blue)
    | '.' -> Some (i, Empty)
    | '_' -> None
    | _ -> fprintf stderr "Json : Wrong format for grid case"; exit (-1)
    )
  | _ -> fprintf stderr "Json : Wrong format for grid row"; exit (-1)

let grid_from_ast ast =
  List.mapi ast ~f:(fun i elt ->
    (i, grid_row_from_ast elt)
  )

let canvas_from_ast ast =
  let h = List.length ast in
  let fst = List.hd ast in
  let w = match fst with
    | Some (`String str) -> String.length str
    | _ -> 0
  in
  (w, h)

let level_from_ast ast : t =
  match ast with
  (* TODO : Allow definition of a level in any order  *)
  | `Assoc [ ("functions", `List functions);
             ("robot_location", `List robot_pos);
             ("robot_orientation", `String robot_dir);
             ("stars", `List stars_pos);
             ("map", `List grid)
           ] ->
    {
      grid = grid_from_ast grid;
      robot = {
        pos = robot_pos_from_ast robot_pos;
        dir = robot_dir_from_ast robot_dir };
      functions = functions_from_ast functions;
      stars = stars_from_ast stars_pos;
      canvas = canvas_from_ast grid
    }
  | _ ->
    fprintf stderr "Json : Unexpected IDs in file";
    exit (-1)

let print_position outx lexbuf =
  let pos = lexbuf.lex_curr_p in
  fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  match Json_parser.prog Json_lexer.read lexbuf with
  | exception SyntaxError msg ->
      fprintf stderr "%a: %s\n" print_position lexbuf msg;
      exit (-1)
  | exception Json_parser.Error ->
      fprintf stderr "%a: syntax error\n" print_position lexbuf;
      exit (-1)
  | None ->
      fprintf stderr "File is empty";
      exit (-1)
  | Some value ->
      printf "%a\n" Json_ast.output_value value;
      value

(* ---------- Functions to retrieve the JSON file ----------  *)

let http_get url =
  Stdio.print_endline "Fetching JSON file...";
  let* { XmlHttpRequest.code = cod; content = msg; _} = XmlHttpRequest.get url in
  if cod = 0 || cod = 200 then
    Lwt.return msg
  else
    fst (Lwt.wait ())

let getfile f =
  Stdio.print_endline "Getting file...";
  try Lwt.return (Sys_js.read_file ~name:f) with
  (* read_file returns a Not_found exception which can't be caught
  because it is deprecated, this is why we call http_get on any exceptions instead *)
| _ -> http_get f

let load_level filename =
  let* file_content = getfile filename in
  let lexbuf = Lexing.from_string file_content in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
  let ast = parse_with_error lexbuf in
  Lwt.return (level_from_ast ast)
