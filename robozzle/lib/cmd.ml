
open! Js_of_ocaml
open! Js_of_ocaml_lwt
open Jsoo_defs

type move =
  | Fwd
  | Left
  | Right

type cmd =
  | Move of move
  | Func of int
  | Empty

type t = {
  cmd : cmd;
  color : Level.color
}

let watchdog_max_thold = 10000
let sleep_duration = 0.1

(* ---------- Functions to move the robot ---------- *)

let pos_fwd dir (x, y) =
  let open Level in
  match dir with
    | Up    -> (x, y - 1)
    | Down  -> (x, y + 1)
    | Left  -> (x - 1, y)
    | Right -> (x + 1, y)

let dir_left dir =
  let open Level in
  match dir with
    | Up    -> Left
    | Down  -> Right
    | Left  -> Down
    | Right -> Up

let dir_right dir =
  let open Level in
  match dir with
    | Up    -> Right
    | Down  -> Left
    | Left  -> Up
    | Right -> Down

let move_robot (level : Level.t) move =
  let dir = level.robot.dir in
  let pos = level.robot.pos in
  let (new_dir, new_pos) = match move with
    | Left  -> (dir_left dir,  pos)
    | Right -> (dir_right dir, pos)
    | Fwd   -> (dir,           pos_fwd dir pos)
  in
  { level with robot = { pos = new_pos; dir = new_dir } }

let remove_star (level : Level.t) =
  { level with stars = List.filter (fun pos -> pos <> level.robot.pos) level.stars }

(* ---------- Functions to handle game state ---------- *)

type run_state =
  | Play
  | Pause

type result =
  | Won
  | Lost
  | Ongoing

let game_result (level : Level.t) =
  let (x, y) = level.robot.pos in
  let tile = Adj.get_matrix (y, x) level.grid in
  match tile with
  | None -> Lost
  | Some _ ->
    if (List.length level.stars) = 0 then Won
    else Ongoing

let color_match (level : Level.t) (color : Level.color) =
  (* TODO : Change Adj so that we don't have to always invert x and y, it's error-prone *)
  let (x, y) = level.robot.pos in
  match Adj.get_matrix (y, x) level.grid with
  | None ->
    Stdio.print_endline "Fell of the level ??";
    false (* Happens only if the robot falls off the level, in which case the game is lost anyway *)
  | Some tile_color -> match color with
    | Empty -> true
    | _ -> color = tile_color

let set_run_state run_state new_state _ =
  Stdio.print_endline "----- Call play_pause_game -----";
  run_state := new_state;
  Lwt.return ()

let handle_play_pause run_state play_pause_button step_button =
  let set_run_state = set_run_state run_state in
  match !run_state with
    | Play -> Lwt.return ()
    | Pause ->
      (* Program the buttons to restart the game on next click, and wait for button click *)
      Stdio.print_endline "Waiting for wakeup...";
      let* _ = Lwt.pick [ Lwt_js_events.click play_pause_button >>= (set_run_state Play);
                          Lwt_js_events.click step_button       >>= (set_run_state Pause) ] in
      Stdio.print_endline "Woken up !";
      (* Reprogram the buttons to pause the game on next click *)
      Lwt.ignore_result( Lwt.pick [ Lwt_js_events.click play_pause_button >>= (set_run_state Pause);
                                    Lwt_js_events.click step_button       >>= (set_run_state Pause) ] );
      Lwt.return ()

let run_game level display_level functions =

  let run_state = ref Pause in
  let play_pause_button = get_element_by_id "play_pause_button" in
  let step_button = get_element_by_id "step_button" in

  let rec run_game_aux level cmds watchdog =
    Stdio.print_endline "Call run_game_aux...";

    (* The watchdog counts the number of recursion level and stops if it exceeds the max threshold *)
    if watchdog = 0 then Lwt.return (Stdio.print_endline "Watchdog timeout !") else

    (* Wait when the game is paused, and handle play/pause/step button clicks *)
    let* _ = handle_play_pause run_state play_pause_button step_button in

    (* Execute the current slot's command *)
    match cmds with
    | [] -> Lwt.return (Stdio.print_endline "[]")
    | hd :: tl ->
      let (refresh, updated_level, updated_cmds) =
        if (color_match level hd.color) = false then
                     (false, level,              tl)
        else match hd.cmd with
        | Empty  ->  (false, level,              tl)
        | Move m ->  (true,  move_robot level m, tl)
        | Func i ->  (false, level,              (List.nth functions i) @ tl)
      in
      (* Remove stars at the robot's current tile *)
      let updated_level = remove_star updated_level in

      (* Refresh level and sleep for a lap *)
      let* _ =
        if refresh = true then begin
            display_level updated_level;
            Lwt_js.sleep sleep_duration
        end else Lwt.return ()
      in

      (* Go to next slot (or exit if game is over) *)
      match game_result updated_level with
      | Won  -> Stdio.print_endline "Congratulations, you WIN !!"; fst (Lwt.wait ())
      | Lost -> Lwt.return (Stdio.print_endline "You lost... Try again !")
      | Ongoing -> run_game_aux updated_level updated_cmds (watchdog - 1)
  in
  run_game_aux level (List.nth functions 0) watchdog_max_thold
