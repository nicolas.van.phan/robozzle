
open! Js_of_ocaml
open! Js_of_ocaml_lwt
open Jsoo_defs

let slot_border = "2px solid black"
let slot_border_focused = "2px dotted black"
let slot_size = "30px"

type t = {
    cmd : Cmd.cmd ref;
    color : Level.color ref;
    cell : Html.tableCellElement Js.t
}

type slots = {
    matrix : t array array;
    cur_x : int ref;
    cur_y : int ref
}

(* ---------- Change currently selected slot upon click ---------- *)

let click_handler slot_matrix cur_x cur_y x y _ =
  let old_cell = slot_matrix.(!cur_x).(!cur_y).cell in
  let new_cell = slot_matrix.(x).(y).cell in
  old_cell##.style##.border := Js.string slot_border;
  new_cell##.style##.border := Js.string slot_border_focused;
  cur_x := x;
  cur_y := y;
  let soi = string_of_int in
  Stdio.print_endline ("Click on cell " ^ (soi x) ^ " " ^ (soi y) ); Js._false

let get_cur_slot (slots : slots) =
  let {matrix = matrix ; cur_x = cur_x ; cur_y = cur_y} = slots in
  matrix.(!cur_x).(!cur_y)

(* ---------- Change slot content upon key press ---------- *)

let set_slot_color slot color =
  Stdio.print_endline "Call set_slot_color";
  slot.color := color;
  let open Level in
  let new_color = match color with
    | Green  -> green
    | Blue   -> blue
    | Yellow -> yellow
    | Red    -> red
    | Empty  -> white
  in
  slot.cell##.style##.backgroundColor := new_color

let set_slot_command slot command =
  Stdio.print_endline "Call set_slot_command";
  slot.cmd := command;
  let text = match command with
    | Move Fwd   -> "^"
    | Move Left  -> "<"
    | Move Right -> ">"
    | Empty      -> ""
    | Func i     -> "F " ^ string_of_int (i + 1)
  in
  slot.cell##.textContent := Js.some (Js.string text)

let keydown_handler (slots : slots) ev =
  let cur_slot = get_cur_slot slots in
  let set_color = set_slot_color cur_slot in
  let set_cmd = set_slot_command cur_slot in
  let keycode = ev##.keyCode in
  Stdio.print_endline ("Pressed key : " ^ (string_of_int keycode) );
  let nb_func = Array.length slots.matrix in
  let in_bounds nb = nb >= 49 && nb < (49 + nb_func) in
  let int_of_key nb = nb - 49 in
  let open Level in
  let () = match keycode with
  (* TODO : Find a way to use the equivalent of C '#define KEY value' *)
    | 82 (* red *)    -> set_color Red
    | 71 (* green *)  -> set_color Green
    | 66 (* blue   *) -> set_color Blue
    | 89 (* yellow *) -> set_color Yellow
    | 38 (* up     *) -> set_cmd (Move Fwd)
    | 37 (* left   *) -> set_cmd (Move Left)
    | 39 (* right  *) -> set_cmd (Move Right)
    | 8  (* back   *) -> set_color Empty; set_cmd Empty
    | nb when in_bounds nb -> set_cmd (Func (int_of_key nb))
    | _ -> ()
  in Lwt.return ()

(* ---------- Extract functions from slots ----------  *)

let cmd_of_slot (slot : t) : Cmd.t =
  { cmd = !(slot.cmd) ; color = !(slot.color) }

let function_of_slot_row (slow_row : t array) : Cmd.t list =
  Array.to_list (Array.map cmd_of_slot slow_row)

let functions_of_slots (slots : slots) : Cmd.t list list =
  let { matrix = matrix; cur_x = _; cur_y = _ } = slots in
  Array.to_list (Array.map function_of_slot_row matrix)

(* ---------- Create slot matrix ---------- *)

let new_slot html_row _ =
  let cell = Html.createTd doc in
  let cmd = ref Cmd.Empty in
  let color = ref Level.Empty in
  cell##.style##.border := Js.string slot_border;
  cell##.style##.width  := Js.string slot_size;
  cell##.style##.height := Js.string slot_size;
  cell##.style##.textAlign := Js.string "center";
  Dom.appendChild html_row cell;
  { cmd = cmd ; color = color ; cell = cell }

let new_slot_row html_table funcs x =
  let (_, func_len) = List.nth funcs x in
  let html_row = Html.createTr doc in
  Dom.appendChild html_table html_row;
  let html_row_title = Html.createTd doc in
  html_row_title##.textContent := Js.some (Js.string ("F" ^ string_of_int (x + 1)));
  Dom.appendChild html_row html_row_title;
  Array.init func_len (new_slot html_row)

let new_slot_matrix (level : Level.t) html_table =
  let funcs = level.functions in
  let nb_func = List.length funcs in

  (* Create array of slot rows *)
  let slot_matrix = Array.init nb_func (new_slot_row html_table funcs) in
  let (cur_x, cur_y) = (ref 0, ref 0) in

  (* Attach click_handler to each cell *)
  Array.iteri (fun x slot_row ->
    Array.iteri (fun y slot ->
      let cell = slot.cell in
      cell##.onmousedown := Html.handler (click_handler slot_matrix cur_x cur_y x y)
    ) slot_row
  ) slot_matrix;
  { matrix = slot_matrix; cur_x; cur_y }
