
open! Js_of_ocaml
open! Js_of_ocaml_lwt
module Html = Dom_html

(* JSOO syntax shortcuts *)
let doc = Html.document
let ( >>= ) = Lwt.bind
let (let*) = Lwt.bind
let get_element_by_id id = Js.Opt.get (doc##getElementById (Js.string id)) (fun () -> assert false)

let light_grey = Js.string "#cccccc"
let red        = Js.string "#ff4444"
let green      = Js.string "#00bb00"
let blue       = Js.string "#8888ff"
let yellow     = Js.string "#ffff44"
let grey       = Js.string "#444444"
let black      = Js.string "#000000"
let white      = Js.string "#ffffff"
