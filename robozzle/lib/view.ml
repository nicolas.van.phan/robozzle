
open! Js_of_ocaml
open! Js_of_ocaml_lwt
open Jsoo_defs

(* Level config *)
let tile_size = 40.
let i2p i = tile_size *. float_of_int i
let level_bg_color = grey
let img_star        = "data/imgs/star.png"
let img_robot_up    = "data/imgs/robot_up.png"
let img_robot_down  = "data/imgs/robot_down.png"
let img_robot_left  = "data/imgs/robot_left.png"
let img_robot_right = "data/imgs/robot_right.png"

let draw_star ctx x y =
  let img = Html.createImg doc in
  img##.src := (Js.string img_star);
  ctx##drawImage img (i2p x) (i2p y)

let draw_tile ctx x y color =
  let open Level in
  let style = match color with
    | Blue   -> blue
    | Red    -> red
    | Green  -> green
    | Yellow -> yellow
    | Empty  -> light_grey
  in
  ctx##.fillStyle := black;
  ctx##fillRect (i2p y) (i2p x) (tile_size +. 1.) (tile_size +. 1.);
  ctx##.fillStyle := style;
  ctx##fillRect ((i2p y) +. 1.) ((i2p x) +. 1.) (tile_size -. 1.) (tile_size -. 1.);
  ()

let draw_row ctx x row =
  List.iter (fun (y, color) -> draw_tile ctx x y color) row

let draw_map ctx (level : Level.t) =
  List.iter (fun (x, row) -> draw_row ctx x row) level.grid

let draw_stars ctx (level : Level.t) =
  List.iter (fun (x, y) -> draw_star ctx x y) level.stars

(* TODO : Dont reload an img every time you re-draw the map *)
let draw_robot ctx (level : Level.t) =
  let (x, y) = level.robot.pos in
  let img = Html.createImg doc in
  let src = match level.robot.dir with
    | Up ->    (Js.string img_robot_up)
    | Down ->  (Js.string img_robot_down)
    | Left ->  (Js.string img_robot_left)
    | Right -> (Js.string img_robot_right)
  in
  img##.src := src;
  ctx##drawImage img (i2p x) (i2p y)

let draw_background ctx (level : Level.t) =
  let (w, h) = level.canvas in
  ctx##.fillStyle := level_bg_color;
  ctx##fillRect 0. 0. (i2p w) (i2p h)

let draw_level ctx (level : Level.t) =
  draw_background ctx level;
  draw_map ctx level;
  draw_stars ctx level;
  draw_robot ctx level

let create_canvas body w h =
  let canvas = Html.createCanvas doc in
  canvas##.width := w;
  canvas##.height := h;
  Dom.appendChild body canvas;
  canvas##getContext Html._2d_

let get_canvas_dimensions (level : Level.t) =
  let (w, h) = level.canvas in
  let w = w * (int_of_float tile_size) + 20 in
  let h = h * (int_of_float tile_size) + 20 in
  (w, h)

(* Page alignment cannot be done statically in the HTML file
   because the width of the main wrapper element must be set
   and it is known only after loading the level *)
let align_page w =
  let wrapper = get_element_by_id "main_wrapper" in
  let w = Js.string ((string_of_int w) ^ "px") in
  wrapper##.style##.width := w;
  wrapper##.style##.margin := Js.string "auto";
  ()

let get_level_path =
  let url_args = Url.Current.arguments in
  let rec get_url_arg argname args =
    match args with
    | [] -> "level1" (* Default level *)
    | (name, value) :: tl ->
      Stdio.print_endline ("name : " ^ name ^ "; value : " ^ value);
      if String.equal name argname then
        value
      else
        get_url_arg argname tl
  in
  let level_name = get_url_arg "level" url_args in
  "data/levels/" ^ level_name ^ ".json"

let onload _ =
  Lwt.ignore_result (
    let body = get_element_by_id "app" in
    let* level = Level.load_level get_level_path in
    let (w, h) = get_canvas_dimensions level in
    let ctx = create_canvas body w h in
    align_page w;


    (* Display the slots *)
    let table = get_element_by_id "commands" in
    table##.style##.border := Js.string "1px solid black";
    let slots : Slot.slots = Slot.new_slot_matrix level table in

    let reset_button = get_element_by_id "reset_button" in
    let rec main_loop _ =
      draw_level ctx level;
      let* _ = Lwt.pick [ Cmd.run_game level (draw_level ctx) (Slot.functions_of_slots slots);
                          Lwt_js_events.click reset_button >>= (fun _ -> Lwt.return ());
                          Lwt_js_events.keydown doc >>= (Slot.keydown_handler slots) ] in
      main_loop ()
    in
    main_loop ()
  );
  Js._false
