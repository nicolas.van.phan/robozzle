
let () =
  Dream.run
  @@ Dream.logger
  @@ Dream.router [

    Dream.get "/data/**"    (Dream.static "./app/data");
    Dream.get "/"           (Dream.from_filesystem "app" "index.html");
    Dream.get "/index.html" (Dream.from_filesystem "app" "index.html");

  ]
  @@ Dream.not_found
