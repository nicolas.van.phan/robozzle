(*
   Run all the OCaml test suites defined in the project.
*)

let test_suites : unit Alcotest.test list =
  [
    ("Robozzle", Test_robozzle.Test.tests);
  ]

let () = Alcotest.run "proj" test_suites
